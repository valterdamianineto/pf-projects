<style>
/* styling properties of the title */
.title{
    font-size: 45px;
    text-align: center;
}
/* styling properties of subtitles or blocks */
.subTitle{
    font-size: 30px;
    margin-left: 0.5%;
    font-weight: bold;
}
/* menu styling properties or variable identifiers */
.menu{
    font-size: 20px;
    margin-top: 5px;
    margin-right: 0.2%;
    margin-left: 1%;
}
/* styling properties of submenus, or variables loaded from server */
.subMenu{
    font-size: 20px;
    color: darkblue;
}
/* class used for application to items that need to be on the same line */
.inline{
    display: inline-block;
}
/* text box stylization */
.txtAreaTitle{
    font-size: 25px;
    font-weight: bold;
}
.txtArea{
    width: 98%;
    background: white;
    border-radius: 5px;
    padding: 5px;
    text-align: left;
    margin-top: 10px;
    font-size: 16px;
    height: 200px;
    /* margin-left: 1%; */
}
.cards{
    height: 300px;
    width:100%;
    padding:10px;
    text-align: center;
}
.fileCard{
    height: 280px;
    width:49.5%;
    float:right;
}
.fileBox{
    width: 98%;
    background: white;
    border-radius: 5px;
    padding: 5px;
    text-align: center;
    margin-top: 10px;
    margin-left: 1%;

}
.fileLine{
    padding: 10px;
    border-bottom: 2px solid #e2e8dc;
}

.detailBox{
    height: 280px;
    width:49.5%;
    float:left;
}
/* media query used to adapt items to devices with different resolutions */
@media screen and (max-width: 680px) {
    /* when used on small sized devices, items should not be displayed on the same line */
    .inline{
        display: block;
    }
    /* When used on small sized devices, the text box should occupy 100% of the screen width. And the character size must be smaller */
    .txtArea{
        font-size: 14px;
        width: 99%;
        background: white;
        border-radius: 5px;
        margin-left: 1%;
        margin-top: 10px;
        padding: 3px;
    }
}
</style>

<div class="demanda" style="width: 98%">
    <h3 class="title">Solicitação de Serviço</h3>
    <br>
    <div>
        <div class="subTitle inline">Informações detalhadas sobre o Lead Nº</div>
        <div class="subTitle inline"><?php echo $id ?></div>
    </div>
    <br>
    <br>

    <div class="inline" style="width: 98%;">
        <div>
            <div class="subTitle">Informações de contato</div>
            <div>
                <div class="menu inline">Nome: </div>
                <div class="subMenu inline"><?php echo  $contatoNome ?></div>
            </div>
            <div>
                <div class="menu inline">Email: </div>
                <div class="subMenu inline"><?php echo  $emailContato ?></div>   
            </div>
            <div>
                <div class="menu inline">Telefone: </div>
                <div class="subMenu inline"><?php echo  "$dddContato $telefoneContato" ?></div>
            </div>
        </div>
            <br>
        <div>
            <div class="subTitle">Informações de Endereço</div>
            <div>
                <div class="menu inline">UF: </div>
                <div class="subMenu inline"><?php echo $uf ?></div>
            </div>
            <div>
                <div class="menu inline">Cidade:</div>
                <div class="subMenu inline"><?php echo $cidade ?></div>
            </div>
            <div>
                <div class="menu inline">Bairro:</div>
                <div class="subMenu inline"><?php echo $bairro ?></div>
            </div>
            <div>
                <div class="menu inline">Rua:</div>
                <div class="subMenu inline"><?php echo $logradouro ?></div>
            </div>
            <div>
                <div class="menu inline">Nº:</div>
                <div class="subMenu inline"><?php echo $numero ?></div>
            </div>
            <div>
                <div class="menu inline">CEP:</div>
                <div class="subMenu inline"><?php echo $cep ?></div>
            </div>
            <div>
                <div class="menu inline">Complemento:</div>
                <div class="subMenu inline"><?php echo $complemento ?></div>
            </div>
        </div>
            <br>
        <div>
            <div class="subTitle">Informações do serviço</div>
            <div>
                <div class="menu inline">Tipo de serviço:</div>
                <div class="subMenu inline"><?php echo $assunto ?></div>
            </div>
            <div>
                <div class="menu inline">Tipo de Cliente:</div>
                <div class="subMenu inline"><?php echo $tipoCliente ?></div>
            </div>
            <div>
                <div class="menu inline">Nº de piscinas:</div>
                <div class="subMenu inline"><?php echo $qtdPiscinas ?></div>
            </div>
            <div>
                <div class="menu inline">Volume total das piscinas:</div>
                <div class="subMenu inline"><?php echo $volume ?></div>
            </div>
            <div>
                <div class="menu inline">Visitas semanais desejadas:</div>
                <div class="subMenu inline"><?php echo $visitasSemanais ?></div>
            </div>
            <div>
                <div class="menu inline">Produtos inclusos:</div>
                <div class="subMenu inline"><?php echo $produtosInclusos ?></div>
            </div>
        </div>
            <br>

<div class="cards">
    <div class="detailBox inline">
        <div class="txtAreaTitle">Detalhes sobre o serviço:</div>
        <div style="margin-bottom: 10px;">
            <div class="subMenu txtArea inline"><?php echo $detalhes ?></div>
        </div>
    </div>

        <?php 
if(count($valuesArquivos)>0){
    echo "                
        <div class='fileCard inline'>
            <div class='txtAreaTitle'>Arquivos</div>
                <div class='fileBox'>";
                        foreach($valuesArquivos as $linha) {
                            $id_fk = $linha["id_fk"];
                            $nome= $linha["nome"];
                            $extensao= $linha["extensao"];
                            $qtd= $linha["qtd"];
                echo "
                    <div class='fileLine'>
                        <div>
                            <a target='_blank' href='arquivos/leads/" . $id_fk . "numero" . $qtd . "." . $extensao . "'>" . $qtd . " " . $nome .  "</a>
                        </div>
                    </div>";
                        }
                echo "
                </div>";
                    }
                ?>
            </div>
        </div>
