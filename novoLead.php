<?php
include 'connections.php';
include 'session.php';

$pesqLocal = $pdo->prepare("SELECT id, local FROM locais ORDER BY local");
$pesqLocal->execute();
$values = $pesqLocal->fetchAll();

?>

    <!DOCTYPE html>
    <html>
        <head>
            <title>Novo Lead</title>
            <?php require_once "head.php";?>
        <script>
            function show(){
                if(document.getElementById("modalidade").selectedIndex == 2){
                    document.getElementById("sitioDiv").style.display ="block";
                    document.getElementById("codigoDiv").style.display ="block";
                }else{
                    document.getElementById("sitioDiv").style.display ="none";
                    document.getElementById("sitio").value ="";
                    document.getElementById("codigoDiv").style.display ="none";
                    document.getElementById("codigo").value ="";
                }
            };
        </script>
        <style>
            #sitioDiv{
                display:none;
            }
            #codigoDiv{
                display:none;
            }
        </style>
        </head>
        <body>
            <?php require_once "navbar.php";?>
            <div class="licitação" >
                <form class="form-container" method=post enctype="multipart/form-data" action="cadastrando_lead" >
                    <div class="form-row">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <h3 for="inputEmail4">Novo Lead</h3>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label>Motivo do Contato:</label>
                            <select name='motivoContato' id='motivoContato' class="form-control" require>
                                <option value= "">Selecione o motivo do contato</option>
                                <option value= "Orçamento">Orçamento</option>
                                <option value= "Duvidas">Duvidas</option>
                                <option value= "Sugestões">Sugestões</option>
                                <option value= "Reclamações">Reclamações</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="assunto">Assunto:</label>
                            <select name='assunto' id='assunto' class="form-control" require>
                                <option value=  "">Selecione o assunto do Lead</option>
                                <option value=  "Tratamento de Piscina">Tratamento de Piscina</option>
                                <option value=  "Manutenção Hidraulica">Manutenção Hidraulica</option>
                                <option value=  "Manutenção Elétrica">Manutenção Elétrica</option>
                                <option value=  "Troca de Areia">Troca de Areia</option>
                                <option value=  "Reforma">Reforma de Piscina</option>
                                <option value=  "Construção">Construção de Piscina</option>
                                <option value=  "Manutenção Elétrica">Manutenção Elétrica</option>
                                <option value=  "Guarda Vidas">Guarda Vidas</option>
                                <option value=  "Franquia">Franquia</option>
                                <option value=  "Curso">Curso</option>
                                <option value=  "Aplicativo">Aplicativo</option>
                                <option value=  "Manutenção de Outros Equipamentos">Manutenção de Outros Equipamentos</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="contatoNome">Contato:</label>
                            <input type = 'text' id='contatoNome' name ='contatoNome' placeholder="Nome do contato" class="form-control" require>
                        </div>
                        <div class="form-group" style="width: 5%; margin-right: 0.3%">
                            <label for="dddContato">DDD:</label>
                            <input type ='number' name = 'dddContato' class="form-control" require>
                        </div>
                        <div class="form-group" style="width: 11.4%;">
                            <label  for="telefoneContato">Telefone:</label>
                            <input type ='number' name = 'telefoneContato' class="form-control" require>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="emailContato">Email:</label>
                            <input type ='email' name = 'emailContato' class="form-control">
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-row">
                        <div class="form-group col-md-1">
                            <label for="uf">UF:</label>
                            <select name='uf' class="form-control" require>
                                <option value= "">UF</option>
                                <option value= "AC">AC</option>
                                <option value= "AL">AL</option>
                                <option value= "AM">AM</option>
                                <option value= "AP">AP</option>
                                <option value= "BA">BA</option>
                                <option value= "CE">CE</option>
                                <option value= "DF">DF</option>
                                <option value= "ES">ES</option>
                                <option value= "GO">GO</option>
                                <option value= "MA">MA</option>
                                <option value= "MG">MG</option>
                                <option value= "MS">MS</option>
                                <option value= "MT">MT</option>
                                <option value= "PA">PA</option>
                                <option value= "PB">PB</option>
                                <option value= "PE">PE</option>
                                <option value= "PI">PI</option>
                                <option value= "PR">PR</option>
                                <option value= "RJ">RJ</option>
                                <option value= "RN">RN</option>
                                <option value= "RO">RO</option>
                                <option value= "RR">RR</option>
                                <option value= "RS">RS</option>
                                <option value= "SC">SC</option>
                                <option value= "SE">SE</option>
                                <option value= "SP">SP</option>
                                <option value= "TO">TO</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cidade">Cidade:</label>
                            <input type = 'text' name = 'cidade' class="form-control" require>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="bairro">Bairro:</label>
                            <input type ='text' name ='bairro' class="form-control" require>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="logradouro">Logradouro:</label>
                            <input type='text' name ='logradouro' class="form-control">
                        </div>
                        <div class="form-group" style="width: 7.5%; margin-right: 0.3%; margin-left: 0.3%">
                            <label for="numero">Nº:</label>
                            <input type='number' name='numero' class="form-control">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cep">CEP:</label>
                            <input type='number' name='cep' class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="complemento">Complemento:</label>
                            <input type='text' name='complemento' class="form-control">
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="tipoCliente">Tipo de Cliente:</label>
                            <select name='tipoCliente' id='tipoCliente' class="form-control">
                                <option value= "">Selecione o tipo de Cliente</option>
                                <option value= "Residencia">Residêncial</option>
                                <option value="Condominio">Condomínio</option>
                                <option value= "Clube Recreativo">Clube Recreativo</option>
                                <option value= "Academia">Academia</option>
                                <option value= "Clinica">Clínica </option>
                                <option value= "Escola">Escola </option>
                            </select>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="qtdPiscinas">Piscinas (qtd):</label>
                            <input type = 'text' name = 'qtdPiscinas' class="form-control">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="volume">Volume Aproximado: (m³)</label>
                            <input type = 'number' name = 'volume' class="form-control">
                        </div>
                        <div class="form-group col-md-1">
                            <label for="visitasSemanais">Visitas Semanais:</label>
                            <input type = 'number' name = 'visitasSemanais' class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" style="width: 24.5%; margin-right: 0.3%; margin-left: 0.3%">
                            <label for="produtosInclusos">Produtos Inclusos:</label>
                            <select name='produtosInclusos' id="produtosInclusos" class="form-control">
                                <option value= "">Selecione a opção</option>
                                <option value= "Sim">Sim</option>
                                <option value="Não">Não</option>
                            </select>
                        </div>
                        <div class="form-group" style="width: 24.5%; margin-right: 0.3%; margin-left: 0.3%">
                            <label for="primeiroAtendimento">Primeiro Atendimento:</label>
                            <select name='primeiroAtendimento' id="primeiroAtendimento" class="form-control">
                                <option value="">Selecione a opção</option>
                                <option value="Sim">Sim</option>
                                <option value="Não">Não</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" style="width: 49.5%; margin-right: 0.3%; margin-left: 0.3%">
                            <label for="detalhes">Detalhes:</label>
                            <textarea rows="5" name='detalhes' id="detalhes" class="form-control" placeholder="Descreva os detalhes"></textarea>
                        </div>
                    </div>
                    </div>
                <div class="form-row">
                    <div class="form-group" style="width: 15%; margin-left: 0.3%">
                        <div class="file-field">
                            <div class="btn btn btn-success btn-sm b">
                                <input name="upload1[]" type="file" multiple="multiple" class="custom-file"/>
                                <input type = 'text' name = 'arquivo1' class="form-control" placeholder= "Nomeie o arquivo">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="width: 15%; margin-left: 2.2%">
                        <div class="file-field">
                            <div class="btn btn btn-success btn-sm b">
                                <input name="upload2[]" type="file" multiple="multiple" class="custom-file"/>
                                <input type = 'text' name = 'arquivo2' class="form-control" placeholder= "Nomeie o arquivo">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="width: 15%; margin-left: 2.2%">
                        <div class="file-field">
                            <div class="btn btn btn-success btn-sm b">
                                <input name="upload3[]" type="file" multiple="multiple" class="custom-file"/>
                                <input type = 'text' name = 'arquivo3' class="form-control" placeholder= "Nomeie o arquivo">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-success" style="width: 8%; height: 50px; margin-left: 41.7%">Salvar</button>
                <br><br>
            </form>
            </div>
            <?php require_once "footer.php";?>
        </body>
    </html>
