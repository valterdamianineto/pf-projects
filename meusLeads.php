<?php header("Cache-Control: no-cache, must-revalidate"); 

    include 'connections.php';
    include 'session.php';
    
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    
    $tipo= $_GET['tipo'];
    $usuario = $_SESSION['colaborador'];

    if (isset($_POST['locate_codigo']) && !empty($_POST['locate_codigo'] && $_POST['locate_codigo']) !=null){
        $locate_codigo = $_POST['locate_codigo'];
        $filtraCodigo = " AND locate('" . $locate_codigo . "',id)>0";
    }
    
    if (isset($_POST['initial_date']) && !empty($_POST['initial_date'] && $_POST['initial_date']) !=null){
        $initial_date = $_POST['initial_date'];
        $filtraInicio = " AND dtmInicio >= '" . $initial_date . "'";
    }
    
    if (isset($_POST['final_date']) && !empty($_POST['final_date'] && $_POST['final_date']) !=null){
        $final_date = $_POST['final_date'];
        $filtraFim = " AND dtmInicio <= '" . $final_date . " 23:59:59'";
    }
    
    if (isset($_POST['locate_assunto']) && !empty($_POST['locate_assunto'] && $_POST['locate_assunto']) !=null){
        $locate_assunto = $_POST['locate_assunto'];
        $filtraAssunto = " AND locate('" . $locate_assunto . "',assunto)>0";
    }
    
    if (isset($_POST['locate_tipoCliente']) && !empty($_POST['locate_tipoCliente']) && $_POST['locate_tipoCliente'] !=null && $_POST['locate_tipoCliente'] !='Todos'){
        $locate_tipoCliente = $_POST['locate_tipoCliente'];
        $filtraTipoCliente = " AND tipoCliente = '" . $locate_tipoCliente . "'";
    }else{
        $locate_tipoCliente='Todos';
    }

    
    $pesq = $pdo->prepare("
    SELECT `id`, `dtmInicio`, `dtmFim`, `assunto`, `bairro`, `tipoCliente`, `contatoNome` ,`detalhes`, `statusDisputa`, `ganhador`, `statusConversao`, `dddContato`,`telefoneContato`
    FROM `leads`
    WHERE id >0 " .

    $filtraCodigo . 
    $filtraInicio .
    $filtraFim . 
    $filtraAssunto . 
    $filtraTipoCliente . " 
    ORDER BY dtmInicio ASC");

    $pesq->bindValue(':usuario', $usuario);
    $pesq->execute();
    $valuesContratos = $pesq->fetchAll();
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Meus Leads</title>
        <?php require_once "head.php"; ?>
    </head>
    <body>
        <?php require_once "navbar.php"; ?>
        <form class="form-container" method=post enctype="multipart/form-data">
            <div class="row col-sm-10 col-lg-12">
                <div class="col-sm-10 col-lg-4">
                    <h3 class="naMesmaLinha">Meus Leads</h3>
                </div>
            </div>
        </form>
    <br>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%"><button type='button' class='btn btn-outline-primary dropdown-toggle' data-toggle='modal' data-target='#codigo'>Nº</button></th>
                    <th width="6%"><button type='button' class='btn btn-outline-primary dropdown-toggle' data-toggle='modal' data-target='#inicio'>Data</button></th>
                    <th width="12%"><button type='button' class='btn btn-outline-primary dropdown-toggle' data-toggle='modal' data-target='#tipoCliente'>Cliente</button></th>
                    <th width="13%"><button type='button' class='btn btn-outline-primary'>Telefone</button></th>
                    <th width="13%"><button type='button' class='btn btn-outline-primary'>Bairro</button></th>
                    <th width="13%"><button type='button' class='btn btn-outline-primary dropdown-toggle' data-toggle='modal' data-target='#assunto'>Assunto</button></th>
                    <th width="20%"><button type='button' class='btn btn-outline-primary'>Detalhes</button></th>
                    <th width="10%"><button type='button' class='btn btn-outline-primary dropdown-toggle' data-toggle='modal' data-target='#status'>Status</button></th>                    
                    <th width="8%"></th>
                </tr>
            </thead>
        <tbody>
<?php
    
    echo "<!-- Modal -->
        <div class='modal fade' id='codigo' tabindex='-1' role='dialog' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h3>Código</h3>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div class='modal-body'>
                        <form class: 'form-container' method=post enctype='multipart/form-data'>
                            <div class='form-group col-md-12'>
                                <label>Digite o código do contrato</label>
                                <div>
                                    <label>Código:</label>
                                    <input type='number' class='form-control' name = 'locate_codigo' id='locate_codigo' value='" . $locate_codigo . "' >
                                </div>
                                <input type='hidden' name='initial_date' value='" . $initial_date . "'>
                                <input type='hidden' name='final_date' value='" . $final_date . "'>
                                <input type='hidden' name='locate_assunto' value='" . $locate_assunto . "'>
                                <input type='hidden' name='locate_tipoCliente' value='" . $locate_tipoCliente . "'>
                            </div>
                            <button type='submit' class='btn btn-primary'>Confirmar</button>
                            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
    
    echo "<!-- Modal -->
        <div class='modal fade' id='inicio' tabindex='-1' role='dialog' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h3>Data de Início</h3>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div class='modal-body'>
                        <form class: 'form-container' method=post enctype='multipart/form-data'>
                            <div class='form-group col-md-12'>
                                <label>Filtre a data do início:</label>
                                <div>
                                    <label>De:</label>
                                    <input type='date' class='form-control' name = 'initial_date' id='initial_date' value='" . $initial_date . "' >
                                </div>
                                <div>
                                    <label>Até:</label>
                                    <input type='date' class='form-control' name = 'final_date' id='final_date' value='" . $final_date . "' > 
                                </div>
                                
                                <input type='hidden' name='locate_codigo' value='" . $locate_codigo . "'>
                                <input type='hidden' name='locate_assunto' value='" . $locate_assunto . "'>
                                <input type='hidden' name='locate_tipoCliente' value='" . $locate_tipoCliente . "'>

                            </div>
                            <button type='submit' class='btn btn-primary'>Confirmar</button>
                            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

    echo "<!-- Modal -->
        <div class='modal fade' id='assunto' tabindex='-1' role='dialog' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h3>Assunto</h3>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div class='modal-body'>
                        <form class: 'form-container' method=post enctype='multipart/form-data'>
                            <div class='form-group col-md-12'>
                                <label>Digite parte do assunto de interesse</label>
                                <div>
                                    <label>Assunto:</label>
                                    <input type='text' class='form-control' name = 'locate_assunto' id='locate_assunto' value='" . $locate_assunto . "' >
                                </div>
                                <input type='hidden' name='locate_codigo' value='" . $locate_codigo . "'>
                                <input type='hidden' name='initial_date' value='" . $initial_date . "'>
                                <input type='hidden' name='final_date' value='" . $final_date . "'>
                                <input type='hidden' name='locate_tipoCliente' value='" . $locate_tipoCliente . "'>
                            </div>
                            <button type='submit' class='btn btn-primary'>Confirmar</button>
                            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

    echo "<!-- Modal -->
        <div class='modal fade' id='tipoCliente' tabindex='-1' role='dialog' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h3>Clientes</h3>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div class='modal-body'>
                        <form class: 'form-container' method=post enctype='multipart/form-data'>
                            <div class='form-group col-md-12'>
                                <label>Selecione um tipo de cliente:</label>
                                <div>
                                    <select name='locate_tipoCliente' id='locate_tipoCliente' class='form-control'>
                                        <option value='" . $locate_tipoCliente. "'>" . $locate_tipoCliente . "</option>
                                        <option value= ''>Selecione o tipo de Cliente</option>
                                        <option value= 'Residencia'>Residêncial</option>
                                        <option value= 'Condominio'>Condomínio</option>
                                        <option value= 'Clube' Recreativo>Clube Recreativo</option>
                                        <option value= 'Academia'>Academia</option>
                                        <option value= 'Clinica'>Clínica </option>
                                        <option value= 'Escola'>Escola </option>
                                    </select>  
                                </div>
                                <input type='hidden' name='locate_codigo' value='" . $locate_codigo . "'>
                                <input type='hidden' name='initial_date' value='" . $initial_date . "'>
                                <input type='hidden' name='final_date' value='" . $final_date . "'>
                                <input type='hidden' name='locate_assunto' value='" . $locate_assunto . "'>
                            </div>
                            <button type='submit' class='btn btn-primary'>Confirmar</button>
                            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

    echo "<!-- Modal -->
        <div class='modal fade' id='tipoCliente' tabindex='-1' role='dialog' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h3>Status</h3>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div class='modal-body'>
                        <form class: 'form-container' method=post enctype='multipart/form-data'>
                            <div class='form-group col-md-12'>
                                <label>Selecione o status que você deseja:</label>
                                <div>
                                    <select name='locate_tipoCliente' id='locate_tipoCliente' class='form-control'>
                                        <option value='" . $locate_status. "'>" . $locate_status . "</option>
                                        <option value= ''>Selecione o tipo de Cliente</option>
                                        <option value= 'Proposta'>Proposta Enviada</option>
                                        <option value= 'Contato'>Entrar em Contato</option>
                                        <option value= 'Espera'>Lead em Espera </option>
                                    </select>  
                                </div>
                                <input type='hidden' name='locate_codigo' value='" . $locate_codigo . "'>
                                <input type='hidden' name='initial_date' value='" . $initial_date . "'>
                                <input type='hidden' name='final_date' value='" . $final_date . "'>
                                <input type='hidden' name='locate_assunto' value='" . $locate_assunto . "'>
                            </div>
                            <button type='submit' class='btn btn-primary'>Confirmar</button>
                            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";

    foreach($valuesContratos as $linha) {
        
        if(!is_null($linha['dtmFim'])){
            $dtmFim = strftime('%d/%m %H:%M', strtotime($linha['dtmFim']));
        }else{
            $dtmFim = NULL;
        }
        
        $id= $linha['id'];
        $dtmInicio= strftime('%d/%m %H:%M', strtotime($linha['dtmInicio']));
        $contatoNome = $linha['contatoNome'];
        $dddContato = $linha['dddContato'];
        $telefoneContato = $linha['telefoneContato'];
        $bairro = $linha['bairro'];
        $assunto = $linha['assunto'];
        $detalhes = $linha['detalhes'];
        $statusConvercao = $linha['statusConvercao'];
        $ganhador = $linha['ganhador'];
        $statusDisputa = $linha['statusDisputa'];
        $tipoCliente = $linha['tipoCliente'];

        if($ganhador == $usuario){
            $destino = "onclick=window.location.href='verLead?id=$id'";
            echo "  
                <tr>
                    <td style='text-align:center; vertical-align:middle'>" . $id . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . utf8_encode ($dtmInicio) . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . $contatoNome . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . $dddContato . ' ' . $telefoneContato . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . $bairro . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . $assunto . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . $detalhes . "</td>
                    <td style='text-align:center; vertical-align:middle'>" . $statusConvercao . "</td>
                    <td style='vertical-align:middle'>
                        <button style='height: 55px; font-weight: bold' type='button' class='btn btn-outline-success btn-sm btn-block' " . $destino . ">Ver Lead</button>
                    </td>
                </tr>";
        }
    }
?>
        </tbody>
        </table>
        <?php require_once "footer.php"; ?>
    </body>
</html>