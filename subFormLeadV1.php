<style>
/* styling properties of the title */
.title{
    font-size: 45px;
    text-align: center;
}
/* styling properties of subtitles or blocks */
.subTitle{
    font-size: 30px;
    margin-left: 0.5%;
    font-weight: bold;
}
/* menu styling properties or variable identifiers */
.menu{
    font-size: 20px;
    margin-top: 5px;
    margin-right: 0.2%;
    margin-left: 1%;
}
/* styling properties of submenus, or variables loaded from server */
.subMenu{
    font-size: 20px;
    color: darkblue;
}
/* class used for application to items that need to be on the same line */
.inline{
    display: inline-block;
}
/* text box stylization */
.txtArea{
    font-size: 16px;
    height: 150px;
    background: white;
    border-radius: 5px;
    width: 100%;
    margin-left: 1%;
    margin-top: 10px;
    padding: 5px;
}
.imgBox{
    float: right;
    background-color: white;
    border-radius: 5px;
    margin-top: 28px;
    padding: 10px;
}
.displayImg{
    padding: 5px;
    width: 400px;
    height: 400px;
}
/* media query used to adapt items to devices with different resolutions */
@media screen and (max-width: 680px) {
    /* when used on small sized devices, items should not be displayed on the same line */
    .inline{
        display: block;
    }
    /* When used on small sized devices, the text box should occupy 100% of the screen width. And the character size must be smaller */
    .txtArea{
        font-size: 14px;
        width: 99%;
        background: white;
        border-radius: 5px;
        margin-left: 1%;
        margin-top: 10px;
        padding: 3px;
    }
    .imgBox{
        float: right;
        background-color: white;
        border-radius: 5px;
        margin-top: 15px;
        padding: 3px;
    }
    .displayImg{
        padding: 3px;
        width: 250px;
        height: 250px;
    }
}
</style>

<div class="demanda" style="width: 98%">
    <h3 class="title">Solicitação de Serviço</h3>
    <br>
    <div>
        <h2 class="subTitle inline">Informações detalhadas sobre o Lead Nº</h2>
        <div class="subTitle inline"><?php echo $id ?></div>
    </div>
    <br>
    <div class="inline" style="width: 40%;">
        <div>
            <div class="subTitle">Informações de contato</div>
            <div>
                <div class="menu inline">Nome: </div>
                <div class="subMenu inline"><?php echo  $contatoNome ?></div>
            </div>
            <div>
                <div class="menu inline">Email: </div>
                <div class="subMenu inline"><?php echo  $emailContato ?></div>   
            </div>
            <div>
                <div class="menu inline">Telefone: </div>
                <div class="subMenu inline"><?php echo  "$dddContato $telefoneContato" ?></div>
            </div>
        </div>
            <br>
        <div>
            <div class="subTitle">Informações de Endereço</div>
            <div>
                <div class="menu inline">UF: </div>
                <div class="subMenu inline"><?php echo $uf ?></div>
            </div>
            <div>
                <div class="menu inline">Cidade:</div>
                <div class="subMenu inline"><?php echo $cidade ?></div>
            </div>
            <div>
                <div class="menu inline">Bairro:</div>
                <div class="subMenu inline"><?php echo $bairro ?></div>
            </div>
            <div>
                <div class="menu inline">Rua:</div>
                <div class="subMenu inline"><?php echo $logradouro ?></div>
            </div>
            <div>
                <div class="menu inline">Nº:</div>
                <div class="subMenu inline"><?php echo $numero ?></div>
            </div>
            <div>
                <div class="menu inline">CEP:</div>
                <div class="subMenu inline"><?php echo $cep ?></div>
            </div>
            <div>
                <div class="menu inline">Complemento:</div>
                <div class="subMenu inline"><?php echo $complemento ?></div>
            </div>
        </div>
            <br>
        <div>
            <div class="subTitle">Informações do serviço</div>
            <div>
                <div class="menu inline">Tipo de serviço:</div>
                <div class="subMenu inline"><?php echo $assunto ?></div>
            </div>
            <div>
                <div class="menu inline">Tipo de Cliente:</div>
                <div class="subMenu inline"><?php echo $tipoCliente ?></div>
            </div>
            <div>
                <div class="menu inline">Nº de piscinas:</div>
                <div class="subMenu inline"><?php echo $qtdPiscinas ?></div>
            </div>
            <div>
                <div class="menu inline">Volume total das piscinas:</div>
                <div class="subMenu inline"><?php echo $volume ?></div>
            </div>
            <div>
                <div class="menu inline">Visitas semanais desejadas:</div>
                <div class="subMenu inline"><?php echo $visitasSemanais ?></div>
            </div>
            <div>
                <div class="menu inline">Produtos inclusos:</div>
                <div class="subMenu inline"><?php echo $produtosInclusos ?></div>
            </div>
        </div>
            <br>
        <div>
            <div class="subTitle">Detalhes sobre o serviço:</div>
            <div style="margin-bottom: 10px;">
                <div class="subMenu txtArea"><?php echo $detalhes ?></div>
            </div>
        </div>
    </div>
    <div class="imgBox inline">
        <?php
            if(count($valuesArquivos)>1){
                echo "
                <div>
                    <div class='inline'>
                        <img class='displayImg' src='<?php'/>
                    </div>
                    <div class='inline'>
                        <img class='displayImg' src='arquivos/leads/" . $id_fk . "numero" . $qtd . "." . $extensao . "'/>
                    </div>
                </div>
                <div>
                    <div class='inline'>
                        <img class='displayImg' src=''/>
                    </div>
                    <div class='inline'>
                        <img class='displayImg' src=''/>
                    </div>
                </div>
            ";
            }else{
                echo "
                    <div>
                        <div class='inline'>
                            <img class='displayImg' src='./arquivos/leads/imagemPadrao.jpeg'/>
                        </div>
                        <div class='inline'>
                            <img class='displayImg' src='./arquivos/leads/imagemPadrao.jpeg'/>
                        </div>
                    </div>
                    <div>
                        <div class='inline'>
                            <img class='displayImg' src='./arquivos/leads/imagemPadrao.jpeg'/>
                        </div>
                        <div class='inline'>
                            <img class='displayImg' src='./arquivos/leads/imagemPadrao.jpeg'/>
                        </div>
                    </div>
                ";
            }
        ?>
    </div>
</div>

<?php 
    if(count($valuesArquivos)>0){
        echo "
        <table class='table table-striped col-md-6'>
            <thead>
                <tr>
                    <th>Arquivos</th>
                </tr>
            </thead>";

        foreach($valuesArquivos as $linha) {
            $id_fk = $linha["id_fk"];
            $nome= $linha["nome"];
            $extensao= $linha["extensao"];
            $qtd= $linha["qtd"];
    
        echo "  
            <tr>
                <td><a target='_blank' href='arquivos/leads/" . $id_fk . "numero" . $qtd . "." . $extensao . "'>". $qtd . ". " . $nome . "." . $extensao . "</a></td>
            </tr>";
        }
        
        echo "
        </table>";
    }


?>