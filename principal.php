<?php header("Cache-Control: no-cache, must-revalidate"); 

    include 'connections.php';
    include 'session.php';
    
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php require_once "head.php"; ?>
        <style>
            .box {
            }
            
            .subBox {
                text-align: center;
            }
            
            .icons {
                margin: 40px 50px 40px 40px;
            }
            
            .iconImg {
                max-width: 200px;
                cursor:pointer;
            }
            .iconImg:hover{
            }
            
            .inline {
                display: inline-block;
            }

            @media screen and (max-width: 1580px) {
                .icons {
                margin: 20px 20px 20px 20px;
                }
            
                .iconImg {
                max-width: 150px;
                cursor:pointer;
                }
            }

            @media screen and (max-width: 880px) {
                .icons {
                margin: 10px 10px 10px 10px;
                }
        
                .iconImg {
                max-width: 100px;
                cursor:pointer;
                }
            }

            @media screen and (max-width: 530px) {
                .icons {
                margin: 5px 5px 5px 5px;
                }
        
                .iconImg {
                max-width: 80px;
                cursor:pointer;
                }
            }
            @media screen and (max-width: 400px) {
                .icons {
                margin: 25px 5px 5px 5px;
                }
        
                .iconImg {
                max-width: 80px;
                cursor:pointer;
                }
                .inline{
                display: block;
                }
            }
            
        </style>
    </head>
    <body>
        <?php require_once "navbar.php"; ?>
        <div class="box">
            <div class="subBox">
                <div class="icons inline">
                    <img class="iconImg" src="./arquivos/icons/perfil.png" onclick="window.location.href='verCurriculo.php'">
                </div>
                <div class="icons inline">
                    <img id="gameIcon" class="iconImg" src="./arquivos/icons/game.png" onclick="window.location.href='treinamento#'">
                </div>
                <div class="icons inline">
                    <img style="cursor: not-allowed" class="iconImg" src="./arquivos/icons/controleTecnico.png" onclick="window.location.href='controleTecnico#'">
                </div>
            </div>
            <div class="subBox">
                <div class="icons inline">
                    <img class="iconImg" src="./arquivos/icons/leads.png" onclick="window.location.href='leads'">
                </div>
                <div class="icons inline">
                    <img class="iconImg" src="./arquivos/icons/meusLeads.png" onclick="window.location.href='meusLeads'">
                </div>
                <div class="icons inline">
                    <img class="iconImg" src="./arquivos/icons/meusClientes.png" onclick="window.location.href='meusClientes'">
                </div>
            </div>
            <div class="subBox">
                <div class="icons inline">
                    <img class="iconImg" src="./arquivos/icons/tarefas.png" onclick="window.location.href='tarefas'">
                </div>
                <div class="icons inline">
                    <img id="gameIcon" class="iconImg" src="" onclick="window.location.href=''">
                </div>
                <div class="icons inline">
                    <img class="iconImg" src="./arquivos/icons/calculadoras.png" onclick="window.location.href='calculadoras'">
                </div>
            </div>
        </div>
        <?php require_once "footer.php"; ?>
    </body>
</html>