<style>
    /* styling properties of submenus, or variables loaded from server */
    .subMenu{
        font-size: 20px;
        color: darkblue;
    }
    
    /* text box stylization */
    .txtAreaTitle{
        font-size: 25px;
        font-weight: bold;
    }
    .txtArea{
        width: 98%;
        background: white;
        border-radius: 5px;
        padding: 5px;
        text-align: left;
        margin-top: 10px;
        font-size: 16px;
        height: 200px;
        /* margin-left: 1%; */
    }
    .cards{
        height: 300px;
        width:100%;
        padding:10px;
        text-align: center;
    }
    .fileCard{
        height: 280px;
        width:49.5%;
        float:right;
    }
    .fileBox{
        width: 98%;
        background: white;
        border-radius: 5px;
        padding: 5px;
        text-align: center;
        margin-top: 10px;
        margin-left: 1%;

    }
    .fileLine{
        padding: 10px;
        border-bottom: 2px solid #e2e8dc;
    }

    .detailBox{
        height: 280px;
        width:49.5%;
        float:left;
    }
    .rs{
        margin-left: 3%;
    }
    .valor{
        width: 6%;
    }
    .nomeServico{
        width: 21%;
        margin-top: 15px;
    }
    .recorrencia{
        width: 8.5%;
    }
    /* media query used to adapt items to devices with different resolutions */
    @media screen and (max-width: 680px) {
        /* when used on small sized devices, items should not be displayed on the same line */
        .inline{
            display: block;
        }
        /* When used on small sized devices, the text box should occupy 100% of the screen width. And the character size must be smaller */
        .txtArea{
            font-size: 14px;
            width: 99%;
            background: white;
            border-radius: 5px;
            margin-left: 1%;
            margin-top: 10px;
            padding: 3px;
        }
    }

    .modal {
    display: none;
    position: fixed;
    z-index: 1;
    padding-top: 100px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
    }

    .modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 30%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
    }
    @-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
    }

    @keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
    }

    .close {
    font-size: 20px;
    font-weight: bold;
    margin-top: 20px;
    padding: 0px;
    }
    
    .modal-header {
    background-color: #1268c8;   
    }
    .modal-body {padding: 2px 16px;}
    .modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
    }

</style>
<script type='text/javascript'>

    var tratamentoPiscinaPreco = document.getElementById('tratamentoPiscinaPreco');
    var manutencaoEletricaPreco = document.getElementById('manutencaoEletricaPreco');
    var trocaAreiaPreco = document.getElementById('trocaAreiaPreco');
    var manutencaoHidraulicaPreco = document.getElementById('manutencaoHidraulicaPreco');
    var reformaRevestimentoPreco = document.getElementById('reformaRevestimentoPreco');
    var reformaEstruturalPreco = document.getElementById('reformaEstruturalPreco');
    var instalacaoVinilPreco = document.getElementById('instalacaoVinilPreco');
    var visitaTecnicaPreco = document.getElementById('visitaTecnicaPreco');
    var botoeiraEmergenciaPreco = document.getElementById('botoeiraEmergenciaPreco');
    var raloDeFundoPreco = document.getElementById('raloDeFundoPreco');
    var torreEmergenciaPreco = document.getElementById('torreEmergenciaPreco');
    var outroServPreco = document.getElementById('outroServPreco'); 


    function verValor(event){

        var tratamentoPiscinaPreco = document.getElementById('tratamentoPiscinaPreco').value;
        var manutencaoEletricaPreco = document.getElementById('manutencaoEletricaPreco').value;
        var trocaAreiaPreco = document.getElementById('trocaAreiaPreco').value;
        var manutencaoHidraulicaPreco = document.getElementById('manutencaoHidraulicaPreco').value;
        var reformaRevestimentoPreco = document.getElementById('reformaRevestimentoPreco').value;
        var reformaEstruturalPreco = document.getElementById('reformaEstruturalPreco').value;
        var instalacaoVinilPreco = document.getElementById('instalacaoVinilPreco').value;
        var visitaTecnicaPreco = document.getElementById('visitaTecnicaPreco').value;
        var botoeiraEmergenciaPreco = document.getElementById('botoeiraEmergenciaPreco').value;
        var raloDeFundoPreco = document.getElementById('raloDeFundoPreco').value;
        var torreEmergenciaPreco = document.getElementById('torreEmergenciaPreco').value;
        var outroServPreco = document.getElementById('outroServPreco').value; 
        
        // manutencaoEletricaPreco
        // trocaAreiaPreco
        // manutencaoHidraulicaPreco
        // reformaRevestimentoPreco
        // reformaEstruturalPreco
        // instalacaoVinilPreco
        // visitaTecnicaPreco
        // botoeiraEmergenciaPreco
        // raloDeFundoPreco
        // torreEmergenciaPreco
        // outroServPreco     

        if(tratamentoPiscinaPreco != ''){
            var recorrenciaTratamentoPiscina = document.querySelector('input[name='recorrenciaTratamentoPiscina']:checked').value;
            var produtosLimpeza = document.querySelector('input[name='produtosLimpeza']:checked').value;
            console.log('1', 'Tratamento de Piscina', tratamentoPiscinaPreco, recorrenciaTratamentoPiscina, produtosLimpeza);
        }
        if(manutencaoEletricaPreco != ''){
            var recorrenciaManutencaoEletrica = document.querySelector('input[name='recorrenciaManutencaoEletrica']:checked').value;
            var materialManutencaoEletrica = document.querySelector('input[name='materialManutencaoEletrica']:checked').value;
            console.log('2', 'Manutenção Elétrica', manutencaoEletricaPreco, recorrenciaManutencaoEletrica, materialManutencaoEletrica);
        }
        if(trocaAreiaPreco != ''){
            var recorrenciaTrocaAreia = document.querySelector('input[name='recorrenciaTrocaAreia']:checked').value;
            var materialTrocaAreia = document.querySelector('input[name='materialTrocaAreia']:checked').value;
            console.log('3', 'Troca de Areia', trocaAreiaPreco, recorrenciaTrocaAreia, materialTrocaAreia);
        }
        if(manutencaoHidraulicaPreco != ''){
            var recorrenciaManutencaoHidraulica = document.querySelector('input[name='recorrenciaManutencaoHidraulica']:checked').value;
            var materialManutencaoHidraulica = document.querySelector('input[name='materialManutencaoHidraulica']:checked').value;
            console.log('4', 'Manutenção Hidraulica', manutencaoHidraulicaPreco, recorrenciaManutencaoHidraulica, materialManutencaoHidraulica);

        }
        if(reformaRevestimentoPreco != ''){
            var recorrenciaReformaRevestimento = document.querySelector('input[name='recorrenciaReformaRevestimento']:checked').value;
            var materialReformaRevestimento = document.querySelector('input[name='materialReformaRevestimento']:checked').value;
            console.log('5', 'Reforma Revestimento', reformaRevestimentoPreco, recorrenciaReformaRevestimento, materialReformaRevestimento);

        }
        if(reformaEstruturalPreco != ''){
            var recorrenciaReformaEstrutural = document.querySelector('input[name='recorrenciaReformaEstrutural']:checked').value;
            var materialReformaEstrutural = document.querySelector('input[name='materialReformaEstrutural']:checked').value;
            console.log('6', 'Reforma Estrutural', reformaEstruturalPreco, recorrenciaReformaEstrutural, materialReformaEstrutural);

        }
        if(instalacaoVinilPreco != ''){
            var recorrenciaInstalacaoVinil = document.querySelector('input[name='recorrenciaInstalacaoVinil']:checked').value;
            var materialInstalacaoVinil = document.querySelector('input[name='materialInstalacaoVinil']:checked').value;
            console.log('7', 'Instalação Vinil', instalacaoVinilPreco, recorrenciaInstalacaoVinil, materialInstalacaoVinil);

        }
        if(visitaTecnicaPreco != ''){
            var recorrenciaVisitaTecnica = document.querySelector('input[name='recorrenciaVisitaTecnica']:checked').value;
            console.log('8', 'Visita Técnica', recorrenciaVisitaTecnica, recorrenciaVisitaTecnica);

        }
        if(botoeiraEmergenciaPreco != ''){
            var recorrenciaBotoeiraEmergencia = document.querySelector('input[name='recorrenciaBotoeiraEmergencia']:checked').value;
            var materialBotoeiraEmergencia = document.querySelector('input[name='materialBotoeiraEmergencia']:checked').value;
            console.log('9', 'Instalação Botoeira', botoeiraEmergenciaPreco, recorrenciaBotoeiraEmergencia, materialBotoeiraEmergencia);

        }       
        if(raloDeFundoPreco != ''){
            var recorrenciaRaloDeFundo = document.querySelector('input[name='recorrenciaRaloDeFundo']:checked').value;
            var materialRaloDeFundo = document.querySelector('input[name='materialRaloDeFundo']:checked').value;
            console.log('10', 'Instalação Ralo de Fundo', raloDeFundoPreco, recorrenciaRaloDeFundo, materialRaloDeFundo);

        }
        if(torreEmergenciaPreco != ''){
            var recorrenciaTorreEmergencia = document.querySelector('input[name='recorrenciaTorreEmergencia']:checked').value;
            var materialTorreEmergencia = document.querySelector('input[name='materialTorreEmergencia']:checked').value;
            console.log('11', 'Instalação Torre de Emergencia', torreEmergenciaPreco, recorrenciaTorreEmergencia, materialTorreEmergencia);

        }
        if(outroServPreco != ''){
            var recorrenciaOutroServ = document.querySelector('input[name='recorrenciaOutroServ']:checked').value;
            var materialOutroServ = document.querySelector('input[name='materialOutroServ']:checked').value;
            console.log('12', 'Outro Serviço', outroServPreco, recorrenciaOutroServ, materialOutroServ);

        }else{
            
        };
    }

    function valorTotal(){
        
        var valorTratamentoPiscina = parseFloat(document.getElementById('tratamentoPiscinaPreco').value.replace(',','.'));
        var valormanutencaoEletrica = parseFloat(document.getElementById('manutencaoEletricaPreco').value.replace(',','.'));


        var valorTotal = valorTratamentoPiscina + valormanutencaoEletrica;
        // trocaAreiaPreco.value + manutencaoHidraulicaPreco.value + reformaRevestimentoPreco.value + 
        // reformaEstruturalPreco.value + instalacaoVinilPreco.value + visitaTecnicaPreco.value + botoeiraEmergenciaPreco.value + raloDeFundoPreco.value + torreEmergenciaPreco.value + outroServPreco.value;
        // var valorFinal = parseFloat(valorTotal)
        console.log('2', valorTotal);       
        
    }



    function mostrarMaisItens(){
        var mostraMais = document.getElementById('maisItens');
        var mostrarMaisBtn = document.getElementById('mostrarMaisItens');
        var mostrarMenosBtn = document.getElementById('mostrarMenosItens');

        mostraMais.classList.remove('ocultarLista');
        mostraMais.classList.add('mostrarLista');

        mostrarMaisBtn.classList.remove('mostrarLista');
        mostrarMaisBtn.classList.add('ocultarLista');

        mostrarMenosBtn.classList.remove('ocultarLista');
        mostrarMenosBtn.classList.add('mostrarLista');
    }

    function mostrarMenosItens(){
        var mostraMenos = document.getElementById('maisItens');
        var mostrarMaisBtn = document.getElementById('mostrarMaisItens');
        var mostrarMenosBtn = document.getElementById('mostrarMenosItens');

        mostraMenos.classList.remove('mostrarLista');
        mostraMenos.classList.add('ocultarLista');

        mostrarMenosBtn.classList.remove('mostrarLista');
        mostrarMenosBtn.classList.add('ocultarLista');

        mostrarMaisBtn.classList.remove('ocultarLista');
        mostrarMaisBtn.classList.add('mostrarLista');
    }

    function descricaoModal(){
        var modal = document.getElementById('myModal');
        var span = document.getElementsByClassName('close')[0];
        var itemIdByClasse = document.querySelector('.pegarId').id;
        var itemId = document.getElementById(itemIdByClasse)

        modal.style.display = 'block';
        span.onclick = function() {
        modal.style.display = 'none';
        }
        window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
            }
        }
    }
</script>

<div class='demanda' style='width: 98%; background-color: #c8e4f2;' onchange='valorTotal()'>
    <h3 class='title'>Elaboração de Proposta Comercial</h3>
    <br>
    <br>
    <div class='inline' style='width: 98%;'>
        <br>
            <div class='subTitle'>Informe o valor e a descrição dos serviços a serem inseridos na proposta comercial.</div>
        <br>
        <br>
        <!-- Bloco 1 - tratamento de piscina -->
                <div class='form-group'>
                    <div class='inline nomeServico'>
                        <label class='menu' for='tratamentoPiscina'>Tratamento de Piscina</label>
                    </div>
                    <div class='menu inline rs'>R$</div>
                    <input id='tratamentoPiscinaPreco' class='form-control inline valor' placeholder='0,00' onkeydown='FormataMoeda(this,10,event)' onkeypress='return maskKeyPress(event)'>
                    <div id='modalLimpeza' class='modal'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <h2>Descreva o serviço:</h2>
                                <button class='close'>Salvar</button>
                            </div>
                            <div class='modal-body'>
                                <textarea class='descricaoTxt pegarId' type='text' name='descricao' id='descricaoTratamentoPiscina'></textarea>
                            </div>
                        </div>
                    </div>    
                    <script>
                        function modalLimpeza(){
                            var modal = document.getElementById('modalLimpeza');
                            var closeDescricaoLimpeza = document.getElementsByClassName('close')[0];
                            modal.style.display = 'block';
                            closeDescricaoLimpeza.onclick = function() {
                            modal.style.display = 'none';
                            }
                            window.onclick = function(event) {
                            if (event.target == modal) {
                                modal.style.display = 'none';
                            }}}
                    </script>
                </div>

            <div id='maisItens' class='ocultarLista'>
      
            </div>
        </div>
        <br>
        
        <div class='btnProposta'>
            <button>Ver proposta</button>
            <button>Enviar proposta via email</button>
        </div>
        <div>
            <button onclick='verValor()'>Tornar Cliente</button>
        </div>
</div>